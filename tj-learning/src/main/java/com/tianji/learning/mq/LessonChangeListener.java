package com.tianji.learning.mq;

import com.tianji.api.dto.trade.OrderBasicDTO;
import com.tianji.common.constants.MqConstants;
import com.tianji.common.utils.CollUtils;
import com.tianji.learning.service.ILearningLessonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;

@Slf4j
@Component
@RequiredArgsConstructor
public class LessonChangeListener {


    private  final ILearningLessonService lessonService;
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "learning.lesson.pay.queue",durable = "true"),
            exchange = @Exchange(name = MqConstants.Exchange.ORDER_EXCHANGE,type = ExchangeTypes.TOPIC),
            key = MqConstants.Key.ORDER_PAY_KEY

    ))
    public void listenLessonPay(OrderBasicDTO orderBasicDTO){
        // 健壮性处理
        if (orderBasicDTO == null || orderBasicDTO.getUserId()==null || CollUtils.isEmpty(orderBasicDTO.getCourseIds())){
            log.error("接收到mq消息，订单数据为null");
            return;
        }
            // 添加课程
        log.debug("监听到用户{}订单，需要添加课程{}到课程表中",orderBasicDTO.getUserId(),orderBasicDTO.getOrderId(),orderBasicDTO.getCourseIds());
        lessonService.addUserLessons(orderBasicDTO.getUserId(),orderBasicDTO.getCourseIds());

    }
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "learning.lesson.refund.queue",durable = "true"),
            exchange = @Exchange(name = MqConstants.Exchange.ORDER_EXCHANGE,type = ExchangeTypes.TOPIC),
            key = MqConstants.Key.ORDER_REFUND_KEY
    ))
    public void listenLessonRefund(OrderBasicDTO orderBasicDTO){
        if (orderBasicDTO == null || orderBasicDTO.getUserId()==null || CollUtils.isEmpty(orderBasicDTO.getCourseIds())){
            log.error("接收到mq消息，订单数据为null");
            return;
        }

        // 删除数据
        log.debug("监听到用户{}订单，需要删除课程{}到课程表中",orderBasicDTO.getUserId(),orderBasicDTO.getOrderId(),orderBasicDTO.getCourseIds());
        lessonService.deleteClassScheduleLesson(orderBasicDTO.getUserId(),orderBasicDTO.getCourseIds());
    }

}
