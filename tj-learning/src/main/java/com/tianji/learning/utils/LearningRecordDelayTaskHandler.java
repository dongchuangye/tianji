package com.tianji.learning.utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class LearningRecordDelayTaskHandler {

    // @Data
    // @NoArgsConstructor
    // private static class RecordCacheData{
    //     private Long id;
    //     private Integer moment;
    //     private Boolean finished;
    //
    //     public RecordCacheData(LearningRecord record) {
    //         this.id = record.getId();
    //         this.moment = record.getMoment();
    //         this.finished = record.getFinished();
    //     }
    // }
    // @Data
    // @NoArgsConstructor
    // private static class RecordTaskData{
    //     private Long lessonId;
    //     private Long sectionId;
    //     private Integer moment;
    //
    //     public RecordTaskData(LearningRecord record) {
    //         this.lessonId = record.getLessonId();
    //         this.sectionId = record.getSectionId();
    //         this.moment = record.getMoment();
    //     }
    // }
}
