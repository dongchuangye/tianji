package com.tianji.learning.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tianji.learning.service.IInteractionQuestionService;
import com.tianji.learning.domain.po.InteractionQuestion;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 互动提问的问题表 控制器
 * </p>
 *
 * @author author
 */
@Api(tags = "InteractionQuestion管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/interactionQuestion")
public class InteractionQuestionController {

    private final IInteractionQuestionService interactionQuestionService;


}
