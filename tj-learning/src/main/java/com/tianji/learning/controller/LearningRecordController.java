package com.tianji.learning.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tianji.learning.service.ILearningRecordService;
import com.tianji.learning.domain.po.LearningRecord;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 学习记录表 控制器
 * </p>
 *
 * @author author
 */
@Api(tags = "LearningRecord管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/learningRecord")
public class LearningRecordController {

    private final ILearningRecordService learningRecordService;


}
