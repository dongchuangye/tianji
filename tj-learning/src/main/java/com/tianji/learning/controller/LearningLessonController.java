package com.tianji.learning.controller;

import com.tianji.common.domain.dto.PageDTO;
import com.tianji.common.domain.query.PageQuery;
import com.tianji.learning.domain.vo.LearningLessonVO;
import org.springframework.web.bind.annotation.*;
import com.tianji.learning.service.ILearningLessonService;
import com.tianji.learning.domain.po.LearningLesson;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 * 学生课程表 控制器
 * </p>
 *
 * @author author
 */
@Api(tags = "LearningLesson管理")
@RestController
@RequiredArgsConstructor

public class LearningLessonController {

    private final ILearningLessonService learningLessonService;




    @GetMapping("/lessons/page")
    public PageDTO<LearningLessonVO> pageQuery(PageQuery pageQuery){
           return learningLessonService. pageQuery(pageQuery);
    }

    @GetMapping("/lessons/now")
    @ApiOperation("查询最近学习的课程")
    public LearningLessonVO queryMyCurrentLesson(){
        return learningLessonService.queryMyCurrentLesson();
    }


    @GetMapping("/lessons/{courseId}")
    @ApiOperation("查询课程的学习状态")
    public LearningLessonVO queryLearningStateLesson(@PathVariable("courseId") Long courseId){
        return learningLessonService.queryLearningStateLesson(courseId);
    }
    @DeleteMapping("/lessons/{courseId}")
    @ApiOperation("删除课表中的某课程")
    public void deleteClassScheduleLesson(@PathVariable Long courseId){
     List<Long>  cos=  new ArrayList<>();
     cos.add(courseId);
        learningLessonService.deleteClassScheduleLesson(null,cos);
    }

    @GetMapping("/lessons/{courseId}/count")
    @ApiOperation("统计课程学习人数")
    public LearningLessonVO queryLessonNumber(@PathVariable("courseId") Long courseId){
       return learningLessonService.queryLessonNumber(courseId);
    }

    @GetMapping("/lessons/{courseId}/valid")
    @ApiOperation("校验用户是否可以学习当前课程")
    public LearningLessonVO isLessonValid(@PathVariable("courseId") Long courseId){
        return learningLessonService.isLessonValid(courseId);
    }





}
