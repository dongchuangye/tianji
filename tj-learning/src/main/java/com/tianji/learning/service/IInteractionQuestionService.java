package com.tianji.learning.service;

import com.tianji.learning.domain.po.InteractionQuestion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 互动提问的问题表 服务类
 * </p>
 *
 * @author author
 */
public interface IInteractionQuestionService extends IService<InteractionQuestion> {

}
