package com.tianji.learning.service;

import com.tianji.learning.domain.po.PointsBoard;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 学霸天梯榜 服务类
 * </p>
 *
 * @author author
 */
public interface IPointsBoardService extends IService<PointsBoard> {

}
