package com.tianji.learning.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tianji.api.client.course.CatalogueClient;
import com.tianji.api.client.course.CourseClient;
import com.tianji.api.dto.course.CataSimpleInfoDTO;
import com.tianji.api.dto.course.CourseFullInfoDTO;
import com.tianji.api.dto.course.CourseSimpleInfoDTO;
import com.tianji.common.domain.dto.PageDTO;
import com.tianji.common.domain.query.PageQuery;
import com.tianji.common.exceptions.BadRequestException;
import com.tianji.common.exceptions.BizIllegalException;
import com.tianji.common.utils.BeanUtils;
import com.tianji.common.utils.CollUtils;
import com.tianji.common.utils.ObjectUtils;
import com.tianji.common.utils.UserContext;
import com.tianji.learning.domain.po.LearningLesson;
import com.tianji.learning.domain.vo.LearningLessonVO;
import com.tianji.learning.enums.LessonStatus;
import com.tianji.learning.mapper.LearningLessonMapper;
import com.tianji.learning.service.ILearningLessonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 学生课程表 服务实现类
 * </p>
 *
 * @author author
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class LearningLessonServiceImpl extends ServiceImpl<LearningLessonMapper, LearningLesson> implements ILearningLessonService {


    private final LearningLessonMapper learningLessonMapper;


    private final CourseClient courseClient;


    private final CatalogueClient catalogueClient;
    @Override
    public PageDTO<LearningLessonVO> pageQuery(PageQuery pageQuery) {
        //1.判断关键数据
        if (pageQuery.getPageNo()<1){
            pageQuery.setPageNo(PageQuery.DEFAULT_PAGE_NUM);
        }
        if (pageQuery.getPageSize()<1){
            pageQuery.setPageSize(PageQuery.DEFAULT_PAGE_SIZE);
        }
       // 2.构建分页对象
        IPage<LearningLesson> page = new Page<>(pageQuery.getPageNo(),pageQuery.getPageSize());

        //3.构建查询条件对象
        // 拿到用户id 利用线程池的局部变量
        Long userId = UserContext.getUser();
        if (ObjectUtils.isEmpty(userId)){
            throw new BizIllegalException("查不到");
        }

        LambdaQueryWrapper<LearningLesson> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(LearningLesson::getUserId,userId)
                          .orderByDesc(LearningLesson::getLatestLearnTime);


       // 4.查询结果内容
        IPage<LearningLesson> selectPage = learningLessonMapper.selectPage(page, lambdaQueryWrapper);

        long total = selectPage.getTotal();
        List<LearningLesson> records = selectPage.getRecords();
       // 5.封装结果并返回
        if (CollectionUtils.isEmpty(records)){
            return PageDTO.empty((Page<?>) page);
        }else {
            // 将po对象转为vo对象
            List<LearningLessonVO> lessonVOS = BeanUtils.copyList(records, LearningLessonVO.class);

            // 获得课表中的所有id
            Set<Long> collect = lessonVOS.stream().map(learningLessonVO -> learningLessonVO.getCourseId()).collect(Collectors.toSet());

            // 查询课程有效期,利用feign的远程调用
            List<CourseSimpleInfoDTO> simpleInfoList = courseClient.getSimpleInfoList(collect);


            Map<Long, CourseSimpleInfoDTO> map = simpleInfoList.stream().collect(Collectors.toMap(CourseSimpleInfoDTO::getId, courseSimpleInfoDTO -> courseSimpleInfoDTO));


            for (LearningLessonVO lessonVO : lessonVOS) {
                CourseSimpleInfoDTO simpleInfoDTO = map.get(lessonVO.getCourseId());
                if (ObjectUtils.isEmpty(simpleInfoDTO)){
                    log.error("课表的课程数据异常：courseId:{}  learningLessonId:{}",lessonVO.getCourseId(),lessonVO.getId());
                    throw new BizIllegalException("课表的课程数据异常");
                }
                lessonVO.setCourseName(simpleInfoDTO.getName());
                lessonVO.setSections(simpleInfoDTO.getSectionNum());
                lessonVO.setCourseCoverUrl(simpleInfoDTO.getCoverUrl());
            }
            PageDTO<LearningLessonVO> dto = PageDTO.of((Page<?>) page, lessonVOS);

            return dto;
        }

    }

    @Override
    @Transactional
    public void addUserLessons(Long userId, List<Long> courseIds) {
//        0.是否开启事务
//        1.判断关键数据
        //LessonChangeListener中已经做了健壮性判断，已无需判断
//        2.判断业务数据
//        关键数据操作的后端数据  或  操作的后端数据所关联的数据
//        1.判断课程数据
        List<CourseSimpleInfoDTO> simpleInfoList = courseClient.getSimpleInfoList(courseIds);
        if (CollUtils.isEmpty(simpleInfoList)){
            throw new BizIllegalException("用户添加课程数据异常");
        }
//        2.判断用户数据

//        3.业务操作
       List<LearningLesson> list = new ArrayList<>(simpleInfoList.size());
        for (CourseSimpleInfoDTO courseSimpleInfoDTO : simpleInfoList) {
            LearningLesson learningLesson = new LearningLesson();
            // 获取课程有效期
            Integer validDuration = courseSimpleInfoDTO.getValidDuration();
            if (validDuration != null && validDuration>0){
                LocalDateTime time = LocalDateTime.now();
                learningLesson.setCreateTime(time);
                learningLesson.setExpireTime(time.plusMonths(validDuration));
            }
            learningLesson.setUserId(userId);
            learningLesson.setCourseId(courseSimpleInfoDTO.getId());
            list.add(learningLesson);
        }

//        4.返回数据
        saveBatch(list);

    }

    @Override
    public LearningLessonVO queryMyCurrentLesson() {
        // 获取登录用户的id
        Long user = UserContext.getUser();

        if (ObjectUtils.isEmpty(user)){
            throw new BizIllegalException("查不到");
        }
        // 查询正在学习的课程
        LearningLesson lesson = lambdaQuery()
                .eq(LearningLesson::getUserId, user)
                .eq(LearningLesson::getStatus, LessonStatus.LEARNING.getValue())
                .orderByDesc(LearningLesson::getLatestSectionId)
                .last("limit 1")
                .one();
        // 判断课程
        if (lesson == null){
            return null;
        }

        // 拷贝
        LearningLessonVO learningLessonVO = BeanUtils.copyBean(lesson, LearningLessonVO.class);
        // 查询课程信息
        CourseFullInfoDTO courseInfoById = courseClient.getCourseInfoById(lesson.getCourseId(), false, false);

        if (courseInfoById == null){
            throw new BadRequestException("课程不存在");
        }

        learningLessonVO.setCourseName(courseInfoById.getName());
        learningLessonVO.setCourseCoverUrl(courseInfoById.getCoverUrl());
        learningLessonVO.setSections(courseInfoById.getSectionNum());


        // 查询小节

        List<CataSimpleInfoDTO> list = catalogueClient.batchQueryCatalogue(CollUtils.singletonList(lesson.getLatestSectionId()));
        if (!CollUtils.isEmpty(list)){
            CataSimpleInfoDTO cataSimpleInfoDTO = list.get(0);

            learningLessonVO.setLatestSectionName(cataSimpleInfoDTO.getName());
            learningLessonVO.setLatestSectionIndex(cataSimpleInfoDTO.getCIndex());
        }

        return learningLessonVO;
    }

    /**
     * 查询课程的学习状态
     * @param courseId 课程id
     * @return
     */
    @Override
    public LearningLessonVO queryLearningStateLesson(Long courseId) {
       // 1.判断关键数据 courseId是否存在或者是否为空
        if (courseId == null){
            throw new BadRequestException("课程不存在");
        }
        List<CourseSimpleInfoDTO> simpleInfoList = courseClient.getSimpleInfoList(Collections.singleton(courseId));

        // 2.构建查询条件对象
        Long userId = UserContext.getUser();
        if (ObjectUtils.isEmpty(userId)){
            throw new BizIllegalException("查不到该用户");
        }
        //根据课程ID查询数据

        if (simpleInfoList == null) {
            throw new BizIllegalException("课程信息不存在");
        }
        LambdaQueryWrapper<LearningLesson> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(LearningLesson::getUserId,UserContext.getUser())
                .eq(LearningLesson::getCourseId,courseId);

        //3.查询结果,获得learning-lesson信息的po对象
        LearningLesson lesson = getOne(queryWrapper);
        // 4判断业务数据(po)
        if (lesson == null){
            return null;
        }

        //5.将po转化成vo
        LearningLessonVO learningLessonVO = BeanUtils.copyBean(lesson, LearningLessonVO.class);


        return learningLessonVO;
    }

    @Override
    public void deleteClassScheduleLesson(Long id,List<Long> courseId) {
        if (id == null){
            id = UserContext.getUser();
        }
        LambdaQueryWrapper<LearningLesson> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(LearningLesson::getUserId,id);
        lambdaQueryWrapper.eq(LearningLesson::getCourseId,courseId);

        learningLessonMapper.delete(lambdaQueryWrapper);
    }

    /**
     *  统计课程学习人数
     * @param courseId 课程id
     * @return vo
     */
    @Override
    public LearningLessonVO queryLessonNumber(Long courseId) {
        //1. 判断关键数据
        //  判断传入的课程id是否为空，如果为空抛出业务异常；（必要数据）
        if (courseId == null){
            throw new BizIllegalException("课程id不存在");
        }
        Long user = UserContext.getUser();
        //2. 判断业务数据
        // 2.1 查询根据courseId查询learning_lesson表，如果为空，报业务异常；（后端操作数据）
        Integer count = lambdaQuery()
                .eq(LearningLesson::getUserId, user)
                .eq(LearningLesson::getCourseId, courseId)
                .in(LearningLesson::getStatus, LessonStatus.LEARNING.getValue(),
                        LessonStatus.FINISHED.getValue(),
                        LessonStatus.NOT_BEGIN.getValue())
                .count();


        if (count == null){
            throw new BizIllegalException("课程数据异常");
        }
        // 2.1 远程调用查询课程信息，如果为空，抛出业务异常；（关联数据）
        CourseFullInfoDTO courseInfoById = courseClient.getCourseInfoById(courseId, false, false);
        if (courseInfoById == null){
            throw new BizIllegalException("课程信息异常");
        }
        //3. 统计查询
        // sql：select count（userId）from learning_lesson where course_id = ？

        LearningLessonVO learningLessonVO = BeanUtils.copyBean(count, LearningLessonVO.class);

        //4. 返回数据

        return learningLessonVO;
    }

    /**
     * 校验用户是否可以学习当前课程
     *
     * @param courseId 课程id
     * @return
     */
    @Override
    public LearningLessonVO  isLessonValid(Long courseId) {
        //获取登录用户ID
        Long userId = UserContext.getUser();

        //通过userID和courseId进行查询过滤
        if (courseId == null){
            throw new BizIllegalException("课程不存在");
        }
        //对课程表进行判断,
        LambdaQueryWrapper<LearningLesson> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(LearningLesson::getUserId,userId);
        queryWrapper.eq(LearningLesson::getCourseId,courseId);



        LearningLesson lesson = getOne(queryWrapper);
        if (lesson == null){
            throw new BizIllegalException("课程表不存在");
        }
        //判断课程是否失效,
//
//        if (lesson.getExpireTime()){
//            throw new BizIllegalException("课程已过期");
//        }
        //判断课程状态
        if (lesson.getStatus() == 3){
            throw new BizIllegalException("课程已过期");
        }
        //判断是否建立了学习计划
        if (lesson.getPlanStatus() == 0){
            throw new BizIllegalException("课程还未有学习计划");
        }
        //合法返回lessonID,否则返回null
        LearningLessonVO learningLessonVO = BeanUtils.copyBean(lesson, LearningLessonVO.class);
        return learningLessonVO;

    }

    private LambdaQueryWrapper<LearningLesson> buildUserIdAndCourseIdWrapper(Long userId, Long courseId) {
        LambdaQueryWrapper<LearningLesson> queryWrapper = new QueryWrapper<LearningLesson>()
                .lambda()
                .eq(LearningLesson::getUserId, userId)
                .eq(LearningLesson::getCourseId, courseId);
        return queryWrapper;
    }
}
