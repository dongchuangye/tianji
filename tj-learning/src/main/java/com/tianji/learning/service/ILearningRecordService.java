package com.tianji.learning.service;

import com.tianji.learning.domain.po.LearningRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 学习记录表 服务类
 * </p>
 *
 * @author author
 */
public interface ILearningRecordService extends IService<LearningRecord> {

}
