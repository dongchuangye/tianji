package com.tianji.learning.service;

import com.tianji.common.domain.dto.PageDTO;
import com.tianji.common.domain.query.PageQuery;
import com.tianji.learning.domain.po.LearningLesson;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tianji.learning.domain.vo.LearningLessonVO;

import java.util.List;

/**
 * <p>
 * 学生课程表 服务类
 * </p>
 *
 * @author author
 */
public interface ILearningLessonService extends IService<LearningLesson> {
    /**
     * 分页查询课表数据
     * @param pageQuery 分页条件
     * @return 返回分页相关数据
     */
    PageDTO<LearningLessonVO> pageQuery(PageQuery pageQuery);

    /**
     * 添加用户列表
     * @param userId 用户id
      * @param courseIds 课程id
     */
    void addUserLessons(Long userId, List<Long> courseIds);

    /**
     * 查询用户最近课表数据
     * @return LearningLessonVO
     */
    LearningLessonVO queryMyCurrentLesson();

    /**
     * 查询课程状态
     * @param courseId 课程id
     * @return vo
     */
    LearningLessonVO queryLearningStateLesson(Long courseId);

    /**
     * 删除课表中的某课程
     * @param courseId
     */
    void deleteClassScheduleLesson(Long id,List<Long> courseId);

    /**
     *  统计课程学习人数
     * @param courseId 课程id
     * @return vo
     */
    LearningLessonVO queryLessonNumber(Long courseId);

    /**
     * 校验用户是否可以学习当前课程
     *
     * @param courseId 课程id
     * @return vo
     */
    LearningLessonVO isLessonValid(Long courseId);
}
