package com.tianji.remark.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tianji.remark.domain.po.LikedRecord;

/**
 * <p>
 * 点赞记录表 服务类
 * </p>
 *
 * @author 虎哥
 */
public interface ILikedRecordService extends IService<LikedRecord> {
}
