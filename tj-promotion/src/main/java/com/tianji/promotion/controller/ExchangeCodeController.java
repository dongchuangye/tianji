package com.tianji.promotion.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 兑换码 控制器
 * </p>
 *
 * @author 虎哥
 */
@Api(tags = "优惠券相关接口")
@RestController
@RequiredArgsConstructor
@RequestMapping("/codes")
public class ExchangeCodeController {
}
